from django.conf.urls import url
from django.urls import include,path
from django.views.generic import RedirectView
from .views import index,get_api

urlpatterns = [
    path('',index, name='books'),
    path('api/',get_api,name='api')
]