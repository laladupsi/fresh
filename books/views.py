from django.shortcuts import render
from django.http import JsonResponse
import requests

# Create your views here.
response={}

def index(request):
    return render(request,'books.html',response)

def get_api(request):
    url="https://www.googleapis.com/books/v1/volumes?q=quilting"
    json_page = requests.get(url).json()
    return JsonResponse(json_page)