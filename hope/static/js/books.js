$(function(){
    $.ajax({
        url: "/books/api",
        success: function(result){
            //console.log(result);
            for(var i=0;i<result.items.length;i++){
                var book = result.items[i];
                var title=book.volumeInfo.title;
                var desc = book.volumeInfo.description.slice(0,150)+"...";
                var image = book.volumeInfo.imageLinks.smallThumbnail;

                $(".container").append('<div class="row center-align"><div class="col s4">\
                  <img src='+image+' alt="HELLO"></div>\
              <div class="col s4">\
                <b>'+title+'</b>\
                <p>'+desc+'</p></div>\
              <div class="col s4">\
                <div class="btn-floating btn-small">\
                  <i class="material-icons">star_border</i>\
                </div>\
              </div></div>');
            }

            var favCount =0;
            $(".btn-floating").click(function(){
              $(this).children().html(
                $(this).children().html()=='star_border' ? 'star':'star_border'
              );
              favCount+=$(this).children().html() == 'star' ? 1 : -1;
              console.log(favCount);
              $("#favCount").html("<h5>Favorite books: "+favCount+"</h5>");
            });
        }
      })});

