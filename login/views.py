from django.shortcuts import render
from django.http import HttpResponseRedirect

# Create your views here.
response = {}

def index(request):
    return render(request,'login.html',response)

def redirect(request):
    return HttpResponseRedirect('/books/')