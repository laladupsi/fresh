$(document).ready(function () {
    var isLight = true;
    $("span").click(function () {
        if (isLight === true) {
            darkBG();
            isLight = false;
        } else {
            lightBG();
            isLight = true;
        }
    });
});

function darkBG() {
    $("body").css("background", "black");
    $("#theme-header").css("color", "white");
};

function lightBG() {
    $("body").css("background", "white");
    $("#theme-header").css("color", "black");
};
$(function () {
    $("#accordion").accordion({
        collapsible: true,
        heightstyle: "content"
    });
});

var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 3000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}

/**
 * $("button").click(function(){
        $.ajax({
            url: "https://enterkomputer.com/api/product/drone.json",
            success: function(result){
                console.log(result);
            }});
        });
 * 
 */
