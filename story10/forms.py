from django import forms

class Subscribe_Form(forms.Form):
    name = forms.CharField(label='name',required=True, max_length=60)
    email=forms.EmailField(required=True)
    password=forms.CharField(label='password',required=True, max_length=20)