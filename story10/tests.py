from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

# Create your tests here.
class Story10UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)
    
    def test_using_index_func(self):
        found = resolve('/register/')
        self.assertEqual(found.func, index)
    
    def test_using_template(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response,'register.html')