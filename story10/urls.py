from django.conf.urls import url
from django.urls import include,path
from django.views.generic import RedirectView
from .views import index,post

urlpatterns = [
    path('',index, name='story10'),
    path('signup/',post,name='post')
]